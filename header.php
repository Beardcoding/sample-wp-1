<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
      <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>
    <?php /* <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png"> */ ?>
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>

    <div id="wrapper">
      <div class="w1">
        <header id="header">
          <div class="container">
            <?php
            $header_logo           = get_field('header_logo', 'options');
            $header_logo_fb_helper = '';
            $header_logo_fb        = get_field('header_logo_fb', 'options');
            if ($header_logo_fb) {
              $header_logo_fb_helper = 'onerror="this.onerror=null; this.src=\'' . $header_logo_fb['sizes']['w90h28'] . '\'"';
            }
            ?>

            <div class="header-holder">
              <?php if ($header_logo || $header_logo_fb_helper): ?>
                <div class="logo">
                  <a href="<?php echo esc_url(home_url('/')); ?>"><img 
                      src="<?php echo $header_logo['sizes']['w90h28'] ?>" 
                      alt="" width="90" height="28"
                      <?php echo $header_logo_fb_helper ?>></a>
                </div>
              <?php endif; ?>
              <div class="links-holder">
                <?php
                $social_facebook_url = get_field('social_facebook_url', 'options');
                $social_ints_url     = get_field('social_ints_url', 'options');
                ?>
                <?php if ($social_facebook_url || $social_ints_url): ?>
                  <ul class="social-networks">
                    <?php if ($social_facebook_url): ?>
                      <li><a class="icon-facebook" target="_blank" 
                             href="<?php echo esc_url($social_facebook_url) ?>"></a></li>
                      <?php endif; ?>
                      <?php if ($social_ints_url): ?>
                      <li><a class="icon-instagram" target="_blank" 
                             href="<?php echo esc_url($social_ints_url) ?>"></a></li>
                      <?php endif; ?>
                  </ul>
                <?php endif; ?>

                <?php
                $cart               = WC()->cart->get_cart();
                ?>
                <a href="#" class="nav-opener icon-basket">
                  <span class="num" id="cart-items-amount"><?php echo sizeof($cart) ?></span>
                </a>

              </div>
            </div>
          </div>
        </header>




        <div class="nav-slide">
          <div class="slide-holder">
            <a href="#" class="nav-opener"><i class="close"></i></a>

            <?php
            $title_sp           = get_field('title_sp', 'options');
            $empty_cart_text_sp = get_field('empty_cart_text_sp', 'options');
            global $woocommerce;
            ?>
            <?php if ($title_sp): ?>
              <h2><?php echo $title_sp ?></h2>
            <?php endif; ?>

            <div class="cart-side">


              <?php if (sizeof($cart) == 0): ?>
                <?php if ($empty_cart_text_sp): ?>
                  <p><?php echo $empty_cart_text_sp ?></p>
                <?php endif; ?>
              <?php else: ?>

                <?php
                foreach ($cart as $cart_item_key => $cart_item) {
                  $_product = wc_get_product($cart_item['data']->get_id());
                  echo "<b>" . $_product->get_title() . '</b>  <br> Quantity: ' . $cart_item['quantity'] . '<br>';
                  $price    = get_post_meta($cart_item['product_id'], '_price', true);
                  echo "  Price: ";
                  echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key) . '<div class="cart_s"></div>'; // PHPCS: XSS ok.
                }
                ?>
              <?php endif; ?>


                  <a  class="btn" href="<?php echo get_permalink( wc_get_page_id( 'cart' ) ); ?>"><?php _e('Go to Cart', 'am') ?></a>
            </div><!-- cart-side -->


          </div>
        </div>