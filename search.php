<?php get_header(); ?>
<div class="main-block">
  <div class="container">
    <div id="content">
      <div class="title">
        <h1 class="page-title"><?php _e('Search for: ', 'am');
echo get_search_query(); ?></h1>
      </div>
<?php if (have_posts()) : ?>

        <div class="posts-list">

          <?php while (have_posts()) : the_post(); ?>

            <?php get_template_part('template-parts/content', 'post'); ?>

  <?php endwhile; ?>

        </div>

        <?php get_template_part('template-parts/pagination', 'post'); ?>

      <?php else : ?>

        <?php get_template_part('template-parts/content', 'none'); ?>

<?php endif; ?>
    </div><!-- content -->

  </div>
</div>

<?php get_footer(); ?>