<?php

// WooCommerce section
function mytheme_add_woocommerce_support() {
  add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'mytheme_add_woocommerce_support');

add_action('woocommerce_cart_is_empty', 'am_open_div_cart_empty', 1);
add_action('woocommerce_before_cart', 'am_open_div_cart', 1);
add_action('woocommerce_before_checkout_form', 'am_open_div_checkout', 1);
add_action('woocommerce_before_account_navigation', 'am_open_div_my_acc_nav', 1);
add_action('woocommerce_before_customer_login_form', 'am_open_div_my_acc_nav', 1);

function am_open_div_my_acc_nav() {
  echo '<div class="my-acc-nav-wrap">';
  echo '<div class="container entry-content">';
  echo '<h1 class="entry-title">' . wc_page_endpoint_title(get_the_title()) . '</h1>';
}

function am_open_div_cart_empty() {
  echo '<div class="cart-empty-wrap">';
  echo '<div class="container">';
  echo '<h1 class="entry-title">' . wc_page_endpoint_title(get_the_title()) . '</h1>';
}

function am_open_div_cart() {
  echo '<div class="cart-wrap">';
  echo '<div class="container">';
  echo '<h1 class="entry-title">' . wc_page_endpoint_title(get_the_title()) . '</h1>';
}

function am_open_div_checkout() {
  echo '<div class="checkout-wrap">';
  echo '<div class="container">';
  echo '<h1 class="entry-title">' . wc_page_endpoint_title(get_the_title()) . '</h1>';
}

add_action('woocommerce_cart_is_empty_after', 'am_close_div_cart', 1);
add_action('woocommerce_after_cart', 'am_close_div_cart', 1);
add_action('woocommerce_after_checkout_form', 'am_close_div_cart', 1);
add_action('woocommerce_after_my_account', 'am_close_div_cart', 1);
add_action('woocommerce_after_edit_account_form', 'am_close_div_cart', 1);
add_action('woocommerce_after_edit_account_address_form', 'am_close_div_cart', 1);
add_action('woocommerce_after_account_downloads', 'am_close_div_cart', 1);
add_action('woocommerce_after_account_orders', 'am_close_div_cart', 1);
add_action('woocommerce_view_order', 'am_close_div_cart', 1);
add_action('woocommerce_after_customer_login_form', 'am_close_div_cart', 1);

function am_close_div_cart() {
  echo '</div>';
  echo '</div>';
}

//
//
//add_action('woocommerce_cart_is_empty', 'sample', 1);
//
//function sample() {
//    echo '<h1>hello</h1>';
//}
//get Instagram photo
function am_instagram_api_curl_connect($api_url) {
  $connection_c = curl_init(); // initializing
  curl_setopt($connection_c, CURLOPT_URL, $api_url); // API URL to connect
  curl_setopt($connection_c, CURLOPT_RETURNTRANSFER, 1); // return the result, do not print
  curl_setopt($connection_c, CURLOPT_TIMEOUT, 20);
  $json_return  = curl_exec($connection_c); // connect and get json data
  curl_close($connection_c); // close connection
  return json_decode($json_return); // decode and return
}

class Anchor_Walker extends Walker_Nav_Menu {

  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
    global $wp_query;
    $indent = ( $depth ) ? str_repeat("\t", $depth) : '';

    $class_names = $value       = '';

    $classes = empty($item->classes) ? array() : (array) $item->classes;

    $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));


    $class_names = ' class="' . esc_attr($class_names) . '"';

    $output .= $indent . '<li  id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';

    $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
    $attributes .=!empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
    $attributes .=!empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
    $attributes .=!empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

    $scroll_link_helper = false;

    if (esc_attr($item->url)[0] == '#') {

      $scroll_link_helper = true;
    }


    $prepend     = '';
    $append      = '';
    $description = !empty($item->description) ? '<span>' . esc_attr($item->description) . '</span>' : '';



    if ($depth != 0) {
      $description = $append      = $prepend     = "";
    }

    $svg = get_field('svg', $item);

    $item_output = $args->before;
    if ($scroll_link_helper) {
      $item_output .= '<a' . $attributes . ' class="scroll-link">';
    } else {
      $item_output .= '<a' . $attributes . '>';
    }

    $item_output .= $args->link_before . $prepend . apply_filters('the_title', $item->title, $item->ID) . $append;
    $item_output .= $description . $args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;

    $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
  }

}

// ------------------------------------------------------------
// add constructor variations to cart

add_action('wp_ajax_add_contructor_table', 'add_contructor_table');
add_action('wp_ajax_nopriv_add_contructor_table', 'add_contructor_table');

function add_contructor_table() {

  $pikkaTopVariationID = filter_input(INPUT_POST, 'pikkaTopVariationID');
//  $pikkaBaseVariationID = filter_input(INPUT_POST, 'pikkaBaseVariationID');

  $pikkaTopProductID  = filter_input(INPUT_POST, 'pikkaTopProductID');
  $pikkaBaseProductID = filter_input(INPUT_POST, 'pikkaBaseProductID');


  global $woocommerce;
  $cart_url = $woocommerce->cart->get_cart_url();

  // add top
  if ($pikkaTopVariationID) {

    $top_added = $woocommerce->cart->add_to_cart($pikkaTopProductID, 1, $pikkaTopVariationID, null, null);
  }


  // add base
  if ($pikkaBaseProductID) {

    $base_added = $woocommerce->cart->add_to_cart($pikkaBaseProductID, 1, 0, null, null);
  }

  $cart = WC()->cart->get_cart();

  $arr['response']     = 'Top: ' . $top_added . '; Base: ' . $base_added;
  $arr['success_text'] = __('Added to', 'am') . ' <a href="' . $cart_url . '">' . __('Cart', 'am') . '</a>';
  $arr['cart_amount']  = sizeof($cart);


  ob_start();
  if (sizeof($cart) == 0):
    $empty_cart_text_sp = get_field('empty_cart_text_sp', 'options');
    if ($empty_cart_text_sp):
      ?>
      <p><?php echo $empty_cart_text_sp ?></p>
    <?php endif; ?>
  <?php else: ?>

    <?php
    foreach ($cart as $cart_item_key => $cart_item) {
      $_product = wc_get_product($cart_item['data']->get_id());
      echo "<b>" . $_product->get_title() . '</b>  <br> Quantity: ' . $cart_item['quantity'] . '<br>';
      $price    = get_post_meta($cart_item['product_id'], '_price', true);
      echo "  Price: ";
      echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key) . '<div class="cart_s"></div>'; // PHPCS: XSS ok.
    }

    echo '<a class="btn" href="' . get_permalink(wc_get_page_id('cart')) . '">' . __('Go to Cart', 'am') . '</a>';

  endif;

  $output = ob_get_contents();
  ob_end_clean();

  $arr['cart_list'] = $output;





  echo json_encode($arr);


  exit;
}

// recalc_cart_badge

add_action('wp_ajax_recalc_cart_badge', 'recalc_cart_badge');
add_action('wp_ajax_nopriv_recalc_cart_badge', 'recalc_cart_badge');

function recalc_cart_badge() {

  global $woocommerce;

  $cart = WC()->cart->get_cart();

  $arr['cart_amount'] = sizeof($cart);



  ob_start();
  if (sizeof($cart) == 0):
    $empty_cart_text_sp = get_field('empty_cart_text_sp', 'options');
    if ($empty_cart_text_sp):
      ?>
      <p><?php echo $empty_cart_text_sp ?></p>
    <?php endif; ?>
  <?php else: ?>

    <?php
    foreach ($cart as $cart_item_key => $cart_item) {
      $_product = wc_get_product($cart_item['data']->get_id());
      echo "<b>" . $_product->get_title() . '</b>  <br> Quantity: ' . $cart_item['quantity'] . '<br>';
      $price    = get_post_meta($cart_item['product_id'], '_price', true);
      echo "  Price: ";
      echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key) . '<div class="cart_s"></div>'; // PHPCS: XSS ok.
    }

    echo '<a class="btn" href="' . get_permalink(wc_get_page_id('cart')) . '">' . __('Go to Cart', 'am') . '</a>';

  endif;

  $output = ob_get_contents();
  ob_end_clean();

  $arr['cart_list'] = $output;

  echo json_encode($arr);


  exit;
}

function wpb_woo_my_account_order() {
  $myorder = array(
    'dashboard' => __('Dashboard', 'woocommerce'),
    'orders' => __('Orders', 'woocommerce'),
    'downloads' => __('Downloads', 'woocommerce'),
    'edit-address' => __('Addresses', 'woocommerce'),
    'edit-account' => __('Account Details', 'woocommerce'),
    'customer-logout' => __('Logout', 'woocommerce'),
  );
  return $myorder;
}

add_filter('woocommerce_account_menu_items', 'wpb_woo_my_account_order');

//
//function wpb_woo_endpoint_title( $title, $id ) {
//   $sep = ' | ';
//    $sitetitle = get_bloginfo();
//    
//	 if ( is_wc_endpoint_url( 'view-order' ) ) {
//        $title = __('View Order: ', 'am');    
//    }
//    if ( is_wc_endpoint_url( 'edit-account' ) ) {
//        $title = __('Account Details', 'am'); 
//    }
//    if ( is_wc_endpoint_url( 'edit-address' ) ) {
//        $title = __('Edit Address', 'am'); 
//    }
//    if ( is_wc_endpoint_url( 'downloads' ) ) {
//        $title = __('Downloads', 'am'); 
//    }
//    if ( is_wc_endpoint_url( 'lost-password' ) ) {
//        $title = __('Lost Password', 'am');    
//    }
//    if ( is_wc_endpoint_url( 'customer-logout' ) ) {
//        $title = __('Logout', 'am');   
//    }
//    if ( is_wc_endpoint_url( 'order-pay' ) ) {
//        $title = __('Order Payment', 'am');    
//    }
//    if ( is_wc_endpoint_url( 'orders' ) ) {
//        $title = __('Orders', 'am');    
//    }
//    if ( is_wc_endpoint_url( 'order-received' ) ) {
//        $title = __('Order Received', 'am');   
//    }
//    if ( is_wc_endpoint_url( 'add-payment-method' ) ) {
//        $title = __('Add Payment Method', 'am');   
//    }
//	return $title;
//}
//add_filter( 'the_title', 'wpb_woo_endpoint_title', 10, 2 );


function custom_pagination() {
  global $wp_query;
  $big   = 999999999;
  $pages = paginate_links(array(
    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
    'format' => '?page=%#%',
    'current' => max(1, get_query_var('paged')),
    'total' => $wp_query->max_num_pages,
    'prev_next' => false,
    'type' => 'array',
    'prev_next' => true,
    'prev_text' => '&laquo; Previous',
    'next_text' => 'Next &raquo;',
  ));
  if (is_array($pages)) {
    $current_page = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
    echo '<div class="pagination pagination-full">';
    foreach ($pages as $i => $page) {
      if ($current_page == 1 && $i == 0) {
        echo "<span class='page-numbers current'>$page</span>";
      } else {
        if ($current_page != 1 && $current_page == $i) {
          echo "<span class='page-numbers current'>$page</span>";
        } else {
          echo "$page";
        }
      }
    }
    echo '</div>';
  }
}
?>