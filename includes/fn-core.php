<?php

if (function_exists('get_field')){

	/**
	 * this function is wrapper of get_field function and RETURNS formatted value from ACF field with $html_open and $html_close, also you can add $esc (read more am_esc function description);
	 * if $post_id will be null it will use current $post
	 * you can use this function without checking if value is empty or not
	 * EXAMPLE:
	 * echo am_get_field('phone_number', '<a href="tel:", '">'.__('Call me', 'am').'</a>', $post->ID 'phone');
	 * if field is not empty it will return <a href="tel:+1758422555222">Call me</a>
	 * if field is empty it will return empty string
	 */
	function am_get_field($field_name, $html_open = '', $html_close = '', $post_id = null, $esc = ''){
        
        global $post;
        
        $toReturn = '';
        
        if(!$post_id){
            $post_id = $post->ID;
        }
        
        if($value = get_field($field_name, $post_id)){
			
            $toReturn = $html_open.am_esc($value, $esc).$html_close;
			
        }
        
        return $toReturn;
        
    }

	/**
	 * this function is wrapper of am_get_field function just prints result so am_the_field('phone_number', '<a href="tel:', '">'.__('Call me', 'am').'</a>', $post->ID, 'phone') == echo am_get_field('phone', '<a href="tel:', '">'.__('Call me', 'am').'</a>', $post->ID, 'phone')
	  * you can use this function without checking if value is empty or not
	 */
    function am_the_field($field_name, $html_open = '', $html_close = '', $post_id = null, $esc = ''){
        
        echo am_get_field($field_name, $html_open, $html_close, $post_id, $esc);
        
    }

	/**
	 * this function is wrapper of get_sub_field function and RETURNS formatted value from ACF sub field with $html_open and $html_close, also you can add $esc (read more am_esc function description);
	 * you can use it in ACF row loop https://www.advancedcustomfields.com/resources/have_rows/
	 * you can use this function without checking if value is empty or not
	 * EXAMPLE:
	 * echo am_get_sub_field('phone_number', '<a href="tel:", '">'.__('Call me', 'am').'</a>', 'phone');
	 * if sub field is not empty it will return <a href="tel:+1758422555222">Call me</a>
	 * if sub field is empty it will return empty string
	 */
	function am_get_sub_field($field_name, $html_open = '', $html_close = '', $esc = ''){
       
        $toReturn = '';
        
        if ($value = get_sub_field($field_name)){
            
            $toReturn = $html_open.am_esc($value, $esc).$html_close;
			
        }
        
        return $toReturn;
        
    }

	/**
	 * this function is wrapper of am_get_sub_field just prints result so am_the_sub_field('phone_number', '<a href="tel:', '">'.__('Call me', 'am').'</a>', 'phone') == echo am_get_sub_field('phone', '<a href="tel:', '">'.__('Call me', 'am').'</a>', 'phone')
	  * you can use this function without checking if value is empty or not
	 */
    function am_the_sub_field($field_name, $html_open = '', $html_close = '', $esc = ''){
        
        echo am_get_sub_field($field_name, $html_open, $html_close, $esc);
        
    }
	
	/**
	 This function is for creating link with ACF Link field type
	 EXMAPLE: 
	 
	 Default:
	 * echo am_get_link($link_array);
	 <a href="http://someurl.com">some title</a>
	 
	 Short:
	 * echo am_get_link($link_array, 'btn');
	 <a class="btn" href="http://someurl.com">some title</a>
	 
	 Full
	 * echo am_get_link($link_array, array('class' => 'btn', 'before' => '<span>Before</span>',  'after' => '<span>After</span>', ));
	 <a class="btn" href="http://someurl.com"><span>Before</span>some title<span>After</span></a>
	 
	 */	
	
	function am_get_link($link = array(), $attrs = array()){
		
		$ready_link = '';
		
		if (!empty($link) && is_array($link) && isset($link['url'])) :
		
			$ready_attr = '';
			$after = '';
			$before = '';
		
			if ($attrs) :
		
				if (is_array($attrs)) : 
		
					foreach ($attrs as $akey => $attr) :
						
						if (!in_array($akey, array('after', 'before'))) : 
							
							$ready_attr .= $akey.'="'.esc_attr($attr).'" ';
		
						endif;
		
					endforeach;
					
					if (isset($attrs['before']) && $attrs['before']) :
		
						$before = $attrs['before'];
					
					endif;
		
					if (isset($attrs['after']) && $attrs['after']) :
		
						$after = $attrs['after'];
					
					endif;
					
				else :
		
					$ready_attr = 'class="'.esc_attr($attrs).'"';
		
				endif;
		
			endif;
		
			$ready_link = '<a href="'.esc_url($link['url']).'" '.$ready_attr.' '.(isset($link['target']) && $link['target']? 'target="'.esc_attr($link['target']).'"' : '').'>'.$before.($link['title']? $link['title'] : $link['url']).$after.'</a>';
		
		endif;
		
		return $ready_link;
		
	}
	
	/**
	 * This function is wrapper of am_get_link just prints result function so am_the_link($link_array, 'btn') == echo am_get_link($image_array, 'btn')
	 */	
	function am_the_link($link = array(), $attrs = array()){
		
		echo am_get_link($link, $attrs);
		
	}
}


/**
 * This function is for escaping strings
 * You can use it everywhere
 * Exmaple:
 * $phone = '+1 (758) 422-555-222';
 <a href="tel:<php echo am_esc($phone, 'phone'); ?>"><php echo $phone; ?></a>
 it will return <a href="tel:+1758422555222">+1 (758) 422-555-222</a>
 */
function am_esc($value, $esc){
	try {
		switch ($esc) :
			case 'url':
				$toReturn = esc_url($value);
				break;
			case 'attr':
				$toReturn = esc_attr($value);
				break;
			case 'html':
				$toReturn = esc_html($value);
				break;
			case 'email':
				// $hex_encoding = true
				$toReturn = antispambot($value, true);
				break;
			case 'email2':
				$toReturn = antispambot($value, false);
				break;
			case 'phone':
				$toReturn = str_replace(array('(', ')', ' ', '-', '.', ','), '', $value);
				break;
			case '':
				$toReturn = $value;
				break;
			default:
				throw new Exception();
				break;
		endswitch;
		return $toReturn;
	} catch (Exception $e) {
		$trace = $e->getTrace()[0];
		ob_clean();
		printf(
			__( "Invalid escaping type (%s) provided for am_esc() in %s on the line %s!", 'am' ),
			$esc,
			$trace['file'],
			$trace['line']
		);
		exit();
	}
}


/**
 * This function is for getting value from array with formatting value
 * you can add $esc (read more am_esc function description)
 * EXMAPLE: 
 * echo am_get_array('phone_number', '<a href="tel:', '">'.__('Call me', 'am').'</a>', $array, 'phone');
 * it will use $array['phone_number] and return <a href="tel:+1758422555222">Call me</a>
 * you can use this function without checking if value is empty or not
 */	
function am_get_array($array_key, $html_open = '', $html_close = '', $array = array(), $esc = ''){
	
	$toReturn = '';
    
    if (is_array($array) && isset($array[$array_key]) && $array[$array_key]){
        
        $toReturn = $html_open.am_esc($array[$array_key], $esc).$html_close;
		
    }
    
    return $toReturn;
}


/**
 * This function is wrapper of am_get_array just prints result function so am_the_array('phone_number', '<a href="tel:', '">'.__('Call me', 'am').'</a>', $array, 'phone') == echo am_get_array('phone_number', '<a href="tel:', '">'.__('Call me', 'am').'</a>', $array, 'phone')
 * you can use this function without checking if value is empty or not
 */

function am_the_array($array_key, $html_open = '', $html_close = '', $array = array(), $esc = ''){
	
	echo am_get_array($array_key, $html_open, $html_close, $array, $esc);
	
}


/**
 * Retina 2x plugin supprt: get URL
 */

function am_get_retina($url){
    if(!function_exists('wr2x_get_retina_from_url')){
        return $url;
    }

    $url_temp = wr2x_get_retina_from_url( $url );

    if(!empty($url_temp)){
        return $url_temp;
    }
    else{
    	if(!empty($url)){
	        $uriPath = parse_url($url, PHP_URL_PATH);
	        $info = pathinfo( $uriPath );
	        $no_extension =  basename( $uriPath, '.'.$info['extension'] );
	        $path = $_SERVER['DOCUMENT_ROOT'].$info['dirname'].'/'.$no_extension.'@2x'.'.'.$info['extension'];
	        if(file_exists($path)){
	            $exploded_path = explode('wp-content', $path);
	            if (isset($exploded_path[1]) && $exploded_path[1]){
	                return content_url().$exploded_path[1];
	            }else{
	                return '';
	            }
	        }else{
	            return $url;
	        }
	    }else{
	    	return $url;
	    }
    }
}

/**
 * Retina 2x plugin supprt: return IMG
 */

function am_get_retina_img($url_normal, $class='', $w = '', $h = '', $alt = ''){
    $url_retina = am_get_retina($url_normal);
    $srcset = '';
    if ($url_retina) {
        $srcset = ' srcset="'.esc_url($url_retina).' 2x"';
    }

    $width = '';
    if ($w) {
        $width = ' width="'.esc_attr($w).'"';
    }
    $height = '';
    if ($h) {
        $height = ' height="'.esc_attr($h).'"';
    }
    return '<img src="'.esc_url($url_normal).'"'.$srcset.$width.$height.' alt="'.esc_attr($alt).'" '.($class? ' class="'.esc_attr($class).'" ' : '').'>';
}


/**

	This function is wrapper of am_get_retina_img function.
	EXMAPLE: 

	Default:
	* am_the_retina_img($image_array);
	<img src="$image_array['url]" srcset="am_get_retina($image_array['url']) 2x" alt="$image_array['alt']" width="$image_array['width']" height="$image_array['height']">

	Short:
	* am_the_retina_img($image_array, 'image_size');
	<img src="$image_array['sizes]['image_size']" srcset="am_get_retina($image_array['sizes]['image_size']) 2x" alt="$image_array['alt']" width="$image_array['sizes]['image_size-width']" height="$image_array['sizes]['image_size-height']">

	Full
	* am_the_retina_img($image_array, array('class' => 'main-image', 'size' => 'image_size',  'any-attr' => 'some_value', ));
	<img src="$image_array['sizes]['image_size']" srcset="am_get_retina($image_array['sizes]['image_size']) 2x" alt="$image_array['alt']" width="$image_array['sizes]['image_size-width']" height="$image_array['sizes]['image_size-height']" class="main-image" any-attr="some_value">

	you can override attrs like width, height, alt trough second parametr $args. For example you can remove attr hegiht by setting 'height' => false
*/	

function am_the_retina_img($image = array(), $args = array()){
	
	$ready_img = '';
	
	$url = '';
	$size = 'url';
	$width = '';
	$height = '';
	$alt = '';
	$class = '';
	if ($image) :
	
		if (is_string($args) && $args) : 
			$size = $args;
		elseif (is_array($args) && isset($args['size']) && $args['size']) :
			$size = $args['size'];
		endif;

		if (is_array($image)) :

			if ($size == 'url' && isset($image['url'])) :
				$url = $image['url'];
			elseif (isset($image['sizes'][$size])) :
				$url = $image['sizes'][$size];
			endif;

		elseif (is_string($image) && $image) :
			$url = $image;
		endif;

		if (is_array($args) && isset($args['width'])) :
			$width = $args['width'];
		elseif (is_array($image) && $size == 'url' && isset($image['width'])) :
			$width = $image['width'];
		elseif (is_array($image) && isset($image['sizes'][$size])) :
			$width = $image['sizes'][$size.'-width'];
		endif;

		if (is_array($args) && isset($args['height'])) :
			$height = $args['height'];
		elseif (is_array($image) && $size == 'url' && isset($image['height'])) :
			$height = $image['height'];
		elseif (is_array($image) && isset($image['sizes'][$size])) :
			$height = $image['sizes'][$size.'-height'];
		endif;

		if (is_array($args) && isset($args['alt'])) :
			$alt = $args['alt'];
		elseif (is_array($image) && isset($image['alt'])) :
			$alt = $image['alt'];
		endif;

		if (is_array($args) && isset($args['class'])) :
			$class = $args['class'];
		endif;

		$ready_img = am_get_retina_img($url, $class, $width, $height, $alt);
	
	endif; 
	
	echo $ready_img;
}


/**

	This function returns image htmlfrom given $image_array. You just need to send as first parametr image array, and as second max midth of image, or array where can be maxwidth (Max width), alt or class. By default $args is 100, so that means that max image width by default is 100;
	EXMAPLE: 

	Default:
	* echo am_get_retina_icon($image_array);
	<img src="$image_array['url]" alt="$image_array['alt']" width="lowest ($image_array['width']/ 2) or 100">

	Short:
	* am_get_retina_icon($image_array, 85);
	<img src="$image_array['url]" alt="$image_array['alt']" width="lowest ($image_array['width']/ 2) or 85">

	Full
	* am_get_retina_icon($image_array, array('class' => 'main-image', 'maxwidth' => 85));
	<img src="$image_array['url]" alt="$image_array['alt']" width="lowest ($image_array['width']/ 2) or 85" class="main-image">

	you can override attrs like width, height, alt through second parametr $args. For example you can remove attr height by setting 'height' => false
*/	

function am_get_retina_icon($image = array(), $args = array()){
	$max_width = 100;
	
	$ready_img = '';
	
	$url = '';
	$alt = '';
	$class = '';
	if ($image && is_array($image)) :

		if (isset($image['url']) && $image['url']) :
			$url = $image['url'];
		endif;

		if (is_array($args) && isset($args['maxwidth']) && $args['maxwidth']) :
			$max_width = $args['maxwidth'];
		elseif (is_array($args) && isset($args['width']) && $args['width']) :
			$max_width = $args['width'];
		elseif ((is_string($args) || is_integer($args)) && $args) :
			$max_width = $args;
		endif;

		if (is_array($args) && isset($args['alt'])) :
			$alt = $args['alt'];
		elseif (is_array($image) && isset($image['alt'])) :
			$alt = $image['alt'];
		endif;

		if (is_array($args) && isset($args['class'])) :
			$class = $args['class'];
		endif;
	
		$ready_img = '<img src="'.esc_url($url).'" width="'.esc_attr(min(round($image['width'] / 2), $max_width)).'" '.($class? ' class="'.esc_attr($class).'" ' : '').' alt="'.($alt? esc_attr($alt) : '').'">';
	
	endif; 
	
	return $ready_img;
	
}


/**
	* This function is wrapper of am_get_retina_icon just prints result function so am_the_retina_icon($image_array, 'btn') == echo am_get_retina_icon($image_array, 'btn')
*/	
function am_the_retina_icon($image = array(), $args = array()){
	
	echo am_get_retina_icon($image, $args);
	
}


/**
* This function returns span with normal and retina images. Used as background images.
*/

function am_get_retina_bg($url_normal){
    $url_retina = am_get_retina($url_normal);
    return '<span data-srcset="'.$url_normal.(($url_retina && $url_retina != $url_normal)? ', '.$url_retina.' 2x' : '').'"></span>';
}


/**
* This function prints span with normal and retina images. Used as background images.
*/

function am_the_retina_bg($url_normal){
    echo am_get_retina_bg($url_normal);
}

/**
 * Custom comments for single or page templates
 */

function am_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
	<<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	
		<div class="comment-author vcard">
			<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?>
			<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
		</div>
	
		<?php if ($comment->comment_approved == '0') : ?>
			<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.','am') ?></em>
			<br />
		<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __('%1$s at %2$s','am'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)','am'),'  ','' );
			?>
		</div>

		<div class="entry-comment"><?php comment_text() ?></div>

		<div class="reply">
			<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php
}

/**
 * Browser detection body_class() output
 */
function am_browser_body_class($classes) {
	
	global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone, $is_edge;

	if($is_lynx) $classes[] = 'lynx';
	elseif($is_gecko) $classes[] = 'gecko';
	elseif($is_opera) $classes[] = 'opera';
	elseif($is_NS4) $classes[] = 'ns4';
	elseif($is_safari) $classes[] = 'safari';
	elseif($is_chrome) $classes[] = 'chrome';
	elseif($is_IE) $classes[] = 'ie';
	elseif($is_edge) $classes[] = 'edge';
	else $classes[] = 'unknown';

	if(wp_is_mobile()) $classes[] = 'mobile';
	if($is_iphone) $classes[] = 'iphone';
    
    
if ( is_user_logged_in() ) {
    $classes[] = 'user-logged';
} else {
     $classes[] = 'user-not-logged';
}
	
	return $classes;
}

/**
 * Filter for get_the_excerpt
 */
 
function am_excerpt_more( $more ) {
	return '...';
}


/**
 * Show instead time of post instead of empty string
 */

function am_has_title($title){
	global $post;
	if($title == ''){
		return get_the_time(get_option( 'date_format' ));
	}else{
		return $title;
	}
}

function am_texturize_shortcode_before($content) {
	$content = preg_replace('/\]\[/im', "]\n[", $content);
	return $content;
}

function am_remove_wpautop( $content ) { 
	$content = do_shortcode( shortcode_unautop( $content ) ); 
	$content = preg_replace('#^<\/p>|^<br \/>|<p>$#', '', $content);
	return $content;
}

// unregister all default WP Widgets
function am_unregister_default_wp_widgets() {
    unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Calendar');
    //unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Search');
    unregister_widget('WP_Widget_Text');
    //unregister_widget('WP_Widget_Categories');
    //unregister_widget('WP_Widget_Recent_Posts');
    //unregister_widget('WP_Widget_Recent_Comments');
    //unregister_widget('WP_Widget_RSS');
    //unregister_widget('WP_Widget_Tag_Cloud');
    //unregister_widget('WP_Nav_Menu_Widget');
}

/**
 * Move Comment textarea to the end of the form
 */

function am_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}


/**
 * Get page id by slag
 */

function am_template_page_id($param) {
   $args = array(
       'meta_key' => '_wp_page_template',
       'meta_value' => 'page-templates/' . $param . '.php',
       'post_type' => 'page',
       'post_status' => 'publish'
   );
   $pages = get_pages($args);
   return $pages[0]->ID;
}


/**
 * Return template HTML
 */

function load_template_part($template_name, $part_name = null) {
   ob_start();
   get_template_part($template_name, $part_name);
   $var = ob_get_contents();
   ob_end_clean();
   return $var;
}

/**
 * Add SVG support
 */

function am_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';
	return $mimes;
}

/**
 * Add SVG support - CSS part
 */

function am_svg_thumb_display() {
	echo '<style>
	td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { 
	 width: 100% !important; 
	 height: auto !important; 
	}
	</style>';
}

/**
 * Add Demo Role for security
 */

function am_demo_role(){
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $role_admin = $wp_roles->get_role('administrator');
    //Adding a 'new_role' with all admin caps
    $wp_roles->add_role('demo', __('Demo','am'), $role_admin->capabilities);
    
    $role = get_role( 'demo' );
    $role->remove_cap( 'edit_themes' );
    $role->remove_cap( 'export' );
    $role->remove_cap( 'list_users' );
    $role->remove_cap( 'promote_users' );
    $role->remove_cap( 'switch_themes' );
    $role->remove_cap( 'remove_users' );
    $role->remove_cap( 'delete_themes' );
    $role->remove_cap( 'delete_plugins' );
    $role->remove_cap( 'edit_plugins' );
    $role->remove_cap( 'edit_users' );
    $role->remove_cap( 'create_users' );
    $role->remove_cap( 'delete_users' );
    $role->remove_cap( 'install_themes' );
    $role->remove_cap( 'install_plugins' );
    $role->remove_cap( 'activate_plugins' );
    $role->remove_cap( 'update_plugin' );
    $role->remove_cap( 'update_themes' );
    $role->remove_cap( 'update_core' );
}

/**
 * Change admin logo url
 */

function am_login_logo_url() {
    return home_url('/');
}


// Add Google Map API

function am_acf_google_map_key() {
	$key = get_field('google_map_api', 'option');
	if($key)
    	acf_update_setting('google_api_key', $key);
}
add_action('acf/init', 'am_acf_google_map_key');


/**
* This function prints custom code to <head>.
*/
add_action( 'wp_head', 'am_custom_header_code', 999 );
function am_custom_header_code(){
    $custom_code = get_field( 'custom_code_header', 'options' );
    if( $custom_code !== '' ){
        print $custom_code;
    }
}


/**
* This function prints custom code before closing </body>.
*/

add_action( 'wp_footer', 'am_custom_footer_code', 999 );
function am_custom_footer_code(){
    $custom_code = get_field( 'custom_code_footer', 'options' );
    if( $custom_code !== '' ){
        print $custom_code;
    }
}

/**
 * Theme calls
*/

add_filter('the_title','am_has_title');
add_filter('excerpt_more', 'am_excerpt_more');
add_action( 'login_headerurl', 'am_login_logo_url' );
add_filter( 'comment_form_fields', 'am_move_comment_field_to_bottom' );
add_filter('upload_mimes', 'am_mime_types');
add_action('admin_head', 'am_svg_thumb_display');

// create demo user which can not install plugins and themes
add_action('init', 'am_demo_role');

//disable emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// This theme uses post thumbnails
add_theme_support( 'post-thumbnails' );

add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support('title-tag');

// Add default posts and comments RSS feed links to head
add_theme_support( 'automatic-feed-links' );

// Allow Shortcodes in Sidebar Widgets
add_filter('widget_text', 'do_shortcode');

// loads template file to use some set of parameters in it
function am_load_template( $template, $slug = '', $params = array(), $echo = true ) {
	$html          = '';
	$default_tpl_dir = 'template-parts';
	$template = trim( $template, '/' );
	$temp = get_parent_theme_file_path( $default_tpl_dir.'/'.$template );
	if(is_array($params) && count($params)) {
		extract($params);
	}
	$template = '';
	if (!empty($temp)) {
		if (!empty($slug)) {
			$template = "{$temp}-{$slug}.php";
			if(!file_exists($template)) {
				$template = $temp.'.php';
			}
		} else {
			$template = $temp.'.php';
		}
	}
	if (file_exists( $template )) {
		ob_start();
		require $template;
		$html = ob_get_clean();
	}
	if( $echo ){
		echo $html;
	} else {
		return $html;
	}
}

function am_get_error_log_rows(){
	$toReturn = new error_parser();
	return $toReturn;
}

class error_parser
{
    public $errors_array = array();

    public function __construct()
    {
        $log_file = $this->autodetect_error_log_file();
        if (!$log_file['error'] && file_exists($log_file['file'])) :
	        $this->quantity = 20;
	        $this->_parse($log_file['file']);
	    endif;
        return true;
    }


	function rfgets($handle) {
	    $line = null;
	    $n = 0;

	    if ($handle) {
	        $line = '';

	        $started = false;
	        $gotline = false;

	        while (!$gotline) {
	            if (ftell($handle) == 0) {
	                fseek($handle, -1, SEEK_END);
	            } else {
	                fseek($handle, -2, SEEK_CUR);
	            }

	            $readres = ($char = fgetc($handle));

	            if (false === $readres) {
	                $gotline = true;
	            } elseif ($char == "\n" || $char == "\r") {
	                if ($started)
	                    $gotline = true;
	                else
	                    $started = true;
	            } elseif ($started) {
	                $line .= $char;
	            }
	        }
	    }

	    fseek($handle, 1, SEEK_CUR);

	    return strrev($line);
	}

    function autodetect_error_log_file() {
		require_once(ABSPATH . 'wp-admin/includes/file.php');
		$errorLoggingEnabled = ini_get('log_errors') && (ini_get('log_errors') != 'Off');
		$logFile = ini_get('error_log');
		$toReturn = array();

		if ( !$errorLoggingEnabled) {
			$local_log_file = get_home_path().'error_log';
			if (file_exists($local_log_file)){
				$toReturn['error'] = false;
				$toReturn['file'] = $local_log_file;
			}else{
				$toReturn['error'] = true;
			}
		}else{
			$toReturn['error'] = false;
			$toReturn['file'] = $logFile;
		}

		return $toReturn;
	}

    private function _parse($filename = '') {
    	$handle = @fopen($filename, 'r');
    	if (filesize( $filename ) > 0){
			$current_error = 0;
			$empty_lines = 0;
			$i = 0;
			while ($current_error < $this->quantity && $empty_lines < 15) {
			    $buffer = $this->rfgets($handle);
			    $i++;
			    if (!empty($buffer)){
			    	$first_letter = substr($buffer, 0,1);
			    	if ($first_letter == '['){
				    	$current_error++;
				    	$empty_lines = 0;
				    	$dateArr = array();
			            preg_match('~^\[(.*?)\]~', $buffer, $dateArr);
			            $buffer = str_replace($dateArr[0], "", $buffer);
			            $buffer = trim($buffer);
			            $date = array(
			                "date" => explode(" ", $dateArr[1])[0],
			                "time" => explode(" ", $dateArr[1])[1]
			            );
			            $severity = "";
			            if(strpos($buffer, "PHP Warning") !== false) {
			                $buffer = str_replace("PHP Warning:", "", $buffer);
			                $buffer = trim($buffer);
			                $severity = "WARNING";
			            } elseif(strpos($buffer, "PHP Notice") !== false) {
			                $buffer = str_replace("PHP Notice:", "", $buffer);
			                $buffer = trim($buffer);
			                $severity = "NOTICE";
			            } elseif(strpos($buffer, "PHP Fatal error") !== false) {
			                $buffer = str_replace("PHP Fatal error:", "", $buffer);
			                $buffer = trim($buffer);
			                $severity = "FATAL";
			            } elseif(strpos($buffer, "PHP Parse error") !== false) {
			                $buffer = str_replace("PHP Parse error:", "", $buffer);
			                $buffer = trim($buffer);
			                $severity = "SYNTAX_ERROR";
			            } else {
			                $severity = "UNIDENTIFIED_ERROR";
			            }
			            $message = $buffer;
			            /* Final Array *//* Add nodes while creating them */
			            $finalArray = array(
			                "date" => $date,
			                "severity" => $severity,
			                "message" => $message
			            );
			            array_push($this->errors_array, $finalArray);
				    }else{
				    	$empty_lines++;
				    }
			    }else{
			    	$empty_lines++;
			    }
			}
		}
		fclose($handle);
    }
}

function am_show_acf($show = true){
	if (!$show){
		add_filter('acf/settings/show_admin', '__return_false');
	}
}