<?php
// ACF fields exports 
$dir = dirname(__FILE__).'/../sources/wp/acf-php';

if (file_exists($dir)) {
	$files_in_dir = scandir($dir);
	foreach ($files_in_dir as $file_in_dir) {
		if (!is_dir($file_in_dir)) {
			$file_info = pathinfo($file_in_dir);
			if ($file_info['extension'] == 'php') {
				$filepath = $dir.'/'.$file_in_dir;
				include_once($filepath);
			}
		}
	}
}