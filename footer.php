</div><!-- w1 -->


<footer id="footer">
  <div class="footer-holder">
    <div class="container">

      <?php
      $logo_f    = get_field('logo_f', 'options');
      $logo_f_fb = get_field('logo_f_fb', 'options');

      $logo_f_fb_helper = '';
      if ($logo_f_fb) {
        $logo_f_fb_helper = 'onerror="this.onerror=null; this.src=\'' . $logo_f_fb['sizes']['w88h90'] . '\'"';
      }
      ?>
      <?php if ($logo_f || $logo_f_fb): ?>
        <div class="footer-logo">
          <a href="<?php echo esc_url(home_url('/')); ?>"><img 
              src="<?php echo $logo_f['sizes']['w88h90'] ?>" 
              alt="<?php echo $logo_f['alt'] ?>" 
              width="88" height="90"
              <?php echo $logo_f_fb_helper ?>></a>
        </div>
      <?php endif; ?>

      <div class="row">

        <?php
        $theme_locations = get_nav_menu_locations();
        $menu_obj        = get_term($theme_locations['footermenu'], 'nav_menu');
        $menu_name       = $menu_obj->name;
        ?>

        <?php if (has_nav_menu('footermenu')) : ?>
          <div class="col">
            <?php if ($menu_name): ?>
              <h2><?php echo $menu_name ?></h2>
            <?php endif; ?>
            <?php
            wp_nav_menu(
                array(
                  'theme_location' => 'footermenu',
                  'menu_class' => 'links-list',
                  'menu_id' => '',
                  'container' => '',
                  'depth' => 1,
                  'walker' => new Anchor_Walker(),
            ));
            ?>
          </div>
        <?php endif; ?>




        <div class="col">
          <div class="holder">

            <?php
            $social_facebook_url = get_field('social_facebook_url', 'options');
            $social_ints_url     = get_field('social_ints_url', 'options');
            ?>
            <?php if ($social_facebook_url || $social_ints_url): ?>
              <ul class="social-networks">
                <?php if ($social_facebook_url): ?>
                  <li><a class="icon-facebook" target="_blank" 
                         href="<?php echo esc_url($social_facebook_url) ?>"></a></li>
                  <?php endif; ?>
                  <?php if ($social_ints_url): ?>
                  <li><a class="icon-instagram" target="_blank" 
                         href="<?php echo esc_url($social_ints_url) ?>"></a></li>
                  <?php endif; ?>
              </ul>
            <?php endif; ?>


            <?php if (have_rows('subscribe', 'options')): ?>

              <?php
              while (have_rows('subscribe', 'options')) : the_row();


                $form = get_sub_field('form');
                ?>

                <div class="form-block">
                  <?php am_the_sub_field('title', '<h2>', '</h2>') ?>

                  <?php echo do_shortcode($form) ?>


                </div>

              <?php endwhile;
              ?>

              <?php
            endif;
            ?> 




          </div>
        </div><!-- col -->



      </div>
    </div>
  </div>
</footer>
</div><!-- wrapper -->
<?php wp_footer(); ?>
</body>
</html>