<?php
/*
  Template Name: Blog
  Template Post Type: page
 */

global $more;
$more  = 0;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

get_header();
?>

<div class="main-block">
  <div class="container">
    <div id="content">

      <div class="title">
        <h1><?php $title = get_field('custom_title');
if (!empty($title)) : echo $title;
else : the_title();
endif; ?></h1>
      </div><!-- /title -->

<?php query_posts('post_type=post&paged=' . $paged); ?>

        <?php if (have_posts()) : ?>

        <div class="posts-list">

          <?php while (have_posts()) : the_post(); ?>

    <?php get_template_part('template-parts/content', 'post'); ?>

        <?php endwhile; ?>

        </div>

        <?php get_template_part('template-parts/pagination', 'post'); ?>

      <?php else : ?>

  <?php get_template_part('template-parts/content', 'none'); ?>

    <?php endif;
    wp_reset_query(); ?>

    </div><!-- content -->

<?php get_sidebar('content') ?>
  </div>
</div>

<?php get_footer(); ?>