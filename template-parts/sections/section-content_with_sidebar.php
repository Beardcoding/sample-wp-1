<div class="main-block">
  <div class="container">
    <div id="content">
      <div class="title">
        <h1><?php the_title(); ?></h1>
      </div>
      <div class="entry">
        <?php the_sub_field('content') ?>
      </div>
      <!-- entry -->
    </div><!-- content -->

    <?php get_sidebar('content') ?>
  </div>
</div>
