<?php
$section_id = get_sub_field('section_id');

$add_to_cart_label = get_sub_field('add_to_cart_label');

if ($add_to_cart_label) {
  $add_to_cart_label_text = $add_to_cart_label;
} else {
  $add_to_cart_label_text = __('Add to cart', 'am');
}

$product_for_top_term  = get_sub_field('product_for_top');
$product_for_base_term = get_sub_field('product_for_base');
?>

<form class="product-block" 
<?php if ($section_id): ?>
       id="<?php echo $section_id ?>"
     <?php endif; ?>>
  <div class="container">
    <?php am_the_sub_field('title', '<h2 class="animated-bottom">', '</h2>') ?>



    <?php
    $top_default_name      = '';
    $base_default_name     = '';


    // ----------- get top default
    $arg = array(
      'post_type' => 'product',
      'order' => 'ASC',
      'product_cat' => $product_for_top_term->slug,
      'orderby' => 'menu_order',
      'posts_per_page' => 1
    );

    $the_query = new WP_Query($arg);
    if ($the_query->have_posts()) :

      while ($the_query->have_posts()) : $the_query->the_post();
        $top_default_name = get_the_title();

        $product_for_top        = wc_get_product(get_the_ID());
        $product_variations_top = $product_for_top->get_available_variations();

        $top_default_price      = $product_variations_top[0]['display_price'];
        $top_default_variation  = $product_variations_top[0]['variation_id'];
        $top_default_dimensions = $product_variations_top[0]['dimensions'];


      endwhile;
    endif;
    wp_reset_postdata();

    // ------------- get base default
    $arg = array(
      'post_type' => 'product',
      'order' => 'ASC',
      'product_cat' => $product_for_base_term->slug,
      'orderby' => 'menu_order',
      'posts_per_page' => 1
    );

    $the_query = new WP_Query($arg);
    if ($the_query->have_posts()) :

      while ($the_query->have_posts()) : $the_query->the_post();
        $base_default_name  = get_the_title();
        $product_for_base   = wc_get_product(get_the_ID());
        $base_default_price = $product_for_base->get_price();

      endwhile;
    endif;
    wp_reset_postdata();
    ?>
    <!-- NAMES -->
    <span class="title animated-bottom"><span 
        class="title-top"><?php echo $top_default_name ?></span><span 
        class="plus">+</span><span 
        class="title-base"><?php echo $base_default_name ?></span></span>



    <div class="holder">
      <!-- PRICE and CART -->
      <?php
      $default_summary_price = $top_default_price + $base_default_price;
      $woo_currency          = get_woocommerce_currency_symbol();

      $default_summary_price_formatted = $woo_currency . number_format($default_summary_price, 2, '.', ',');
      ?>
      <span class="price"><?php echo $default_summary_price_formatted ?></span>

      <?php if ($add_to_cart_label): ?>
        <a class="btn" href="#" id="constructor_add_to_cart"><?php echo $add_to_cart_label_text ?></a>
      <?php endif; ?>

    </div>
    <p class="ajax-constructor-response"></p>

    <span class="size" 
          id="slider-current-dimensions">(<span 
        class="size-l"><?php
          echo $top_default_dimensions['length'];
          ?>" L</span><span class="span-x">X</span><span 
        class="size-w"><?php
          echo $top_default_dimensions['width'];
          ?>" W</span><span 
        class="span-x">X</span><span 
        class="size-h"><?php
          echo $top_default_dimensions['height'];
          ?>" H</span>)</span>

    <div class="slider-top">
      <?php
      $num = 1;
      $arg = array(
        'post_type' => 'product',
        'order' => 'ASC',
        'product_cat' => $product_for_top_term->slug,
        'orderby' => 'menu_order',
        'posts_per_page' => -1
      );

      $the_query = new WP_Query($arg);
      if ($the_query->have_posts()) :

        while ($the_query->have_posts()) : $the_query->the_post();
          global $post;
          $num_helper = '';
          if ($num == 1) {
            $num_helper = 'active';
          }
          $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'w928', false);
          ?>

          <div class="slide <?php echo $num_helper ?>">
            <?php if ($image_url): ?>
              <div class="img-holder">
                <img src="<?php echo $image_url[0]; ?>" 
                     srcset="<?php echo am_get_retina($image_url[0]) ?> 2x" 
                     alt="" 
                     width="928">
              </div>
            <?php endif; ?>
          </div>

          <?php
          $num++;
        endwhile;
      endif;
      wp_reset_postdata();
      ?>
    </div><!-- slider-top -->

    <div class="slider-base">

      <?php
      $arg = array(
        'post_type' => 'product',
        'order' => 'ASC',
        'product_cat' => $product_for_base_term->slug,
        'orderby' => 'menu_order',
        'posts_per_page' => -1
      );

      $the_query = new WP_Query($arg);
      if ($the_query->have_posts()) :
        $num = 1;
        while ($the_query->have_posts()) : $the_query->the_post();
          global $post;
          $image_url  = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'w638', false);
          $num_helper = '';
          if ($num == 1) {
            $num_helper = 'active';
          }
          ?>

          <div class="slide <?php echo $num_helper ?>">
            <?php if ($image_url): ?>
              <div class="img-holder">
                <img src="<?php echo $image_url[0]; ?>" 
                     srcset="<?php echo am_get_retina($image_url[0]) ?> 2x" 
                     alt="" 
                     width="638">
              </div>
            <?php endif; ?>
          </div>

          <?php
          $num++;
        endwhile;
      endif;
      wp_reset_postdata();
      ?>

    </div><!-- slider-base -->
  </div><!-- container -->














  <div class="product-settings left-active">
    <div class="left-area">
      <div class="toggle-block">


        <div class="title-area">
          <?php am_the_sub_field('label_1', '<h2>', '</h2>') ?>
          <a href="#" class="opener left"><i class="icon-click"></i></a>
        </div>


        <div class="slide-area">
          <div class="sub-slider-holder">
            <div class="sub-slider-top">




              <?php
              $num = 1;
              $arg = array(
                'post_type' => 'product',
                'order' => 'ASC',
                'product_cat' => $product_for_top_term->slug,
                'orderby' => 'menu_order',
                'posts_per_page' => -1
              );

              $the_query = new WP_Query($arg);
              if ($the_query->have_posts()) :

                while ($the_query->have_posts()) : $the_query->the_post();
                  global $post;
                  $num_helper = '';
                  if ($num == 1) {
                    $num_helper = 'active';
                  }
                  $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'cw142h19', false);

                  $product_for_top        = wc_get_product(get_the_ID());
                  $product_variations_top = $product_for_top->get_available_variations();

                  $top_price        = $product_variations_top[0]['display_price'];
                  $top_variation_id = $product_variations_top[0]['variation_id'];
                  $top_dimensions   = $product_variations_top[0]['dimensions'];
                  ?>

                  <div class="slide <?php echo $num_helper ?>" 
                       data-price="<?php echo $top_price ?>"
                       data-top-dimensions-length="<?php echo $top_dimensions['length'] ?>"
                       data-top-dimensions-width="<?php echo $top_dimensions['width'] ?>"
                       data-top-dimensions-height="<?php echo $top_dimensions['height'] ?>"
                       data-name="<?php echo get_the_title(); ?>"
                       data-productID="<?php echo get_the_ID(); ?>"
                       data-variation="<?php echo $top_variation_id ?>"
                       >
                         <?php if ($image_url): ?>
                      <div class="image-box">
                        <img src="<?php echo $image_url[0]; ?>" 
                             srcset="<?php echo am_get_retina($image_url[0]) ?> 2x" 
                             alt="" 
                             width="142">
                      </div>
                    <?php endif; ?>

                    <div class="text-box">
                      <h3><?php echo get_the_title() ?></h3>
                      <span class="product-info">
                        <span class="size"><span 
                            class="size-l"><?php
                              echo $top_dimensions['length'];
                              ?>" L</span><span 
                            class="span-x">X</span><span 
                            class="size-w"><?php
                              echo $top_dimensions['width'];
                              ?>" W</span><span 
                            class="span-x">X</span><span 
                            class="size-h"><?php
                              echo $top_dimensions['height'];
                              ?>" H</span></span>
                      </span>
                    </div>
                  </div>

                  <?php
                  $num++;
                endwhile;
              endif;
              wp_reset_postdata();
              ?>

            </div><!-- sub-slider-top -->

            <div class="slider-nav">
              <a href="#" class="prev"><?php _e('prev', 'am') ?></a>
              <span class="paging-info"></span>
              <a href="#" class="next"><?php _e('next', 'am') ?></a>
            </div>


          </div>


          <!-- fills with all possible lengths from variations -->
          <div class="side-holder">
            <div class="size-nav">

              <h3><?php _e('Length', 'am') ?></h3>




              <?php
              $num = 1;
              $arg = array(
                'post_type' => 'product',
                'order' => 'ASC',
                'product_cat' => $product_for_top_term->slug,
                'orderby' => 'menu_order',
                'posts_per_page' => -1
              );

              $the_query = new WP_Query($arg);
              if ($the_query->have_posts()) :

                while ($the_query->have_posts()) : $the_query->the_post();
                  global $post;
                  $num_helper = '';
                  if ($num == 1) {
                    $num_helper = 'active-list';
                  }
                  $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'cw142h19', false);

                  $product_for_top        = wc_get_product(get_the_ID());
                  $product_variations_top = $product_for_top->get_available_variations();
                  ?>
                  <ul data-for-product-top="<?php the_ID() ?>"
                      class="product-length-variations <?php echo $num_helper ?>">
                        <?php
                        $num                    = 1;
                        foreach ($product_variations_top as $length_variation):
                          $num_helper   = '';
                          $radio_helper = '';
                          if ($num == 1) {
                            $num_helper   = 'active';
                            $radio_helper = 'checked';
                          }
                          ?>
                      <li class="<?php echo $num_helper ?>"
                          data-variation-price="<?php echo $length_variation['display_price'] ?>"
                          data-variation-id="<?php echo $length_variation['variation_id'] ?>"
                          data-variation-length="<?php echo $length_variation['dimensions']['length'] ?>">
                        <span class="num"><?php echo $length_variation['attributes']['attribute_pa_length'] ?><sup>"</sup></span>
                        <input type="radio" <?php echo $radio_helper ?>
                               class="radio-length-variation"
                               data-product-id="<?php the_ID() ?>"
                               data-variation-price="<?php echo $length_variation['display_price'] ?>"
                               data-variation-id="<?php echo $length_variation['variation_id'] ?>"
                               data-variation-length="<?php echo $length_variation['dimensions']['length'] ?>"
                               data-variation-width="<?php echo $length_variation['dimensions']['width'] ?>"
                               data-variation-height="<?php echo $length_variation['dimensions']['height'] ?>"
                               name="radio-product-<?php the_ID() ?>" />
                      </li>
                      <?php
                      $num++;
                    endforeach;
                    ?>

                  </ul>
                  <?php
                  $num++;
                endwhile;
              endif;
              wp_reset_postdata();
              ?>




            </div>
            <?php
            $remove_top_label     = get_sub_field('remove_top_label');
            $remove_top_link_text = get_sub_field('remove_top_link_text');
            ?>
            <?php if ($remove_top_label || $remove_top_link_text): ?>
              <p class="remove"><?php echo $remove_top_label ?> <?php if ($remove_top_link_text): ?><a class="remove-left" href="#"><?php echo $remove_top_link_text ?></a><?php endif; ?></p>
            <?php endif; ?>
          </div>


          <?php
          $choose_top_label     = get_sub_field('choose_top_label');
          $choose_top_link_text = get_sub_field('choose_top_link_text');
          ?>
          <?php if ($choose_top_label || $choose_top_link_text): ?>
            <div class="remove-block">
              <p><?php echo $choose_top_label ?> <?php if ($choose_top_link_text): ?><a href="#" class="remove-left"><?php echo $choose_top_link_text ?></a><?php endif; ?></p>
            </div>
          <?php endif; ?>
        </div>


      </div>
    </div><!-- top -->




    <div class="right-area">
      <div class="toggle-block"><span></span>

        <div class="title-area">
          <?php am_the_sub_field('label_2', '<h2>', '</h2>') ?>
          <a href="#" class="opener right"><i class="icon-click"></i></a>
        </div>




        <div class="slide-area">
          <div class="sub-slider-holder">
            <div class="sub-slider-base">





              <?php
              $arg = array(
                'post_type' => 'product',
                'order' => 'ASC',
                'product_cat' => $product_for_base_term->slug,
                'orderby' => 'menu_order',
                'posts_per_page' => -1
              );

              $the_query = new WP_Query($arg);
              if ($the_query->have_posts()) :
                $num = 1;
                while ($the_query->have_posts()) : $the_query->the_post();
                  global $post;
                  $num_helper = '';
                  if ($num == 1) {
                    $num_helper = 'active';
                  }
                  $base_default_name = get_the_title();
                  $product_for_base  = wc_get_product(get_the_ID());

                  $base_default_price = $product_for_base->get_price();

                  $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'w110', false);
                  ?>

                  <div class="slide <?php echo $num_helper ?>" 
                       data-productID="<?php echo get_the_ID() ?>"
                       data-price="<?php echo $base_default_price ?>"
                       data-name="<?php echo get_the_title() ?>"
                       data-variation=""
                       >
                         <?php if ($image_url): ?>
                      <div class="image-box">
                        <img src="<?php echo $image_url[0]; ?>" 
                             srcset="<?php echo am_get_retina($image_url[0]) ?> 2x" 
                             alt="" 
                             width="112">
                      </div>
                    <?php endif; ?>

                    <div class="text-box">
                      <h3><?php echo get_the_title() ?></h3>
                      <span class="product-info">
                        <span class="size"><span 
                            class="size-l"><?php
                              echo $product_for_base->get_length();
                              ?>" L</span><span 
                            class="span-x">X</span><span 
                            class="size-w"><?php
                              echo $product_for_base->get_width();
                              ?>" W</span><span 
                            class="span-x">X</span><span 
                            class="size-h"><?php
                              echo $product_for_base->get_height();
                              ?>" H</span></span>
                      </span>
                    </div>
                  </div>

                  <?php
                  $num++;
                endwhile;
              endif;
              wp_reset_postdata();
              ?>





            </div>


            <div class="slider-nav">
              <a href="#" class="prev-base"><?php _e('prev', 'am') ?></a>
              <span class="paging-info-base"></span>
              <a href="#" class="next-base"><?php _e('next', 'am') ?></a>
            </div>

          </div>
          <?php
          $remove_base_label     = get_sub_field('remove_base_label');
          $remove_base_link_text = get_sub_field('remove_base_link_text');
          ?>
          <?php if ($remove_base_label): ?>
            <div class="side-holder">
              <p class="remove"><?php echo $remove_base_label ?> <?php if ($remove_base_link_text): ?><a class="remove-right" href="#"><?php echo $remove_base_link_text ?></a><?php endif; ?></p>
            </div>
          <?php endif; ?>

          <?php
          $choose_base_label     = get_sub_field('choose_base_label');
          $choose_base_link_text = get_sub_field('choose_base_link_text');
          ?>
          <?php if ($choose_base_label || $choose_base_link_text): ?>
            <div class="remove-block">
              <p><?php echo $choose_base_label ?> <?php if ($choose_base_link_text): ?><a class="remove-right" href="#"><?php echo $choose_base_link_text ?></a><?php endif; ?></p>
            </div>
          <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
</form>