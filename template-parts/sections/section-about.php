<?php
$section_id  = get_sub_field('section_id');
$title       = get_sub_field('title');
$text        = get_sub_field('text');
$image_left  = get_sub_field('image_left');
$image_right = get_sub_field('image_right');
?>

<div class="about-block" 
<?php if ($section_id): ?>
       id="<?php echo $section_id ?>"
     <?php endif; ?>>
  <div class="container">

    <?php if ($title || $text): ?>
      <div class="text-frame">
        <?php am_the_sub_field('title', '<h2 class="animated-bottom">', '</h2>') ?>
        <?php am_the_sub_field('text', '<p class="animated-bottom">', '</p>') ?>
      </div>
    <?php endif; ?>

    <?php if (have_rows('items')): ?>

      <div class="items-holder">
        <?php
        while (have_rows('items')) : the_row();


          $image_svg      = get_sub_field('image_svg');
          $image_fallback = get_sub_field('image_fallback');
          $width          = get_sub_field('width');
          $height         = get_sub_field('height');

          $image_fallback_helper = '';
          if ($image_fallback) {
            $image_fallback_helper = 'onerror="this.onerror=null; this.src=\'' . $image_fallback['url'] . '\'"';
          }
          ?>

          <div class="item">
            <?php if ($image_svg || $image_fallback): ?>
              <i class="ico"><img src="<?php echo $image_svg['url'] ?>"
                                  alt="<?php echo $image_svg['alt'] ?>"
                                  width="<?php echo $width ?>" 
                                  height="<?php echo $height ?>" 
                                  <?php echo $image_fallback_helper ?>></i>
              <?php endif; ?>
              <?php am_the_sub_field('title', '<h3 class="animated-bottom">', '</h3>') ?>
              <?php am_the_sub_field('text', '<p class="animated-bottom">', '</p>') ?>
          </div>

        <?php endwhile;
        ?>
      </div>
      <?php
    endif;
    ?> 




  </div>

  <?php if ($image_left): ?>
    <div class="decor01">
      <img class="img-parallax" data-speed="-0.5" 
           src="<?php echo $image_left['sizes']['w185'] ?>" 
           srcset="<?php echo am_get_retina($image_left['sizes']['w185']) ?> 2x" 
           alt="<?php echo $image_left['alt'] ?>" width="185">
    </div>
  <?php endif; ?>
  <?php if ($image_right): ?>
    <div class="decor02">
      <img class="img-parallax" data-speed="0.5" 
           src="<?php echo $image_right['sizes']['w135'] ?>" 
           srcset="<?php echo am_get_retina($image_right['sizes']['w135']) ?> 2x" 
           alt="<?php echo $image_right['alt'] ?>" 
           width="135">
    </div>
  <?php endif; ?>


</div>