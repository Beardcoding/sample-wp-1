<?php
$links = get_sub_field('link');
$image = get_sub_field('image');
?>

<div class="visual">
  <div class="container">
    <div class="text-area">

      <?php am_the_sub_field('subtitle', '<span class="sub-title animated-bottom">', '</span>') ?>

      <?php am_the_sub_field('title', '<h1 class="animated-bottom">', '</h1>') ?>

      <?php if ($links): ?>
        <a class="btn scroll-link"
        <?php if ($links['target']): ?>
             target="<?php echo $links['target'] ?>"
           <?php endif; ?>
           href="<?php echo esc_url($links['url']) ?>"><?php echo $links['title'] ?></a>
         <?php endif; ?>

    </div>
  </div>
  <?php if ($image): ?>
    <div class="bg-stretch">
      <span data-srcset="<?php echo $image['sizes']['w1440'] ?>, <?php echo am_get_retina($image['sizes']['w1440']) ?> 2x"></span>
    </div>
  <?php endif; ?>
</div>