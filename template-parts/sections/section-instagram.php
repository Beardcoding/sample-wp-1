<?php 
$section_id = get_sub_field('section_id');
?>

<div class="social-section" 
<?php if ($section_id): ?>
       id="<?php echo $section_id ?>"
     <?php endif; ?>>
  <?php
  $title           = get_sub_field('title');
  $nickname        = get_sub_field('nickname');
  $social_ints_url = get_field('social_ints_url', 'options');
  ?>
  <?php if ($title): ?>
    <div class="container">
      <?php am_the_sub_field('title', '<h2 class="animated-bottom">', '</h2>') ?>
      <?php if ($nickname && $social_ints_url): ?>
        <a class="link animated-bottom" 
           target="_blank"
           href="<?php echo esc_url($social_ints_url) ?>"><?php echo $nickname ?></a>
         <?php endif; ?>
    </div>
  <?php endif; ?>


  <?php
  $token = get_field('api_token_inst', 'option');
  $id    = get_field('user_id_inst', 'option');
  if (!empty($token) && !empty($id)) {
    $access_token = $token;
    $user_id      = $id;
    $return       = am_instagram_api_curl_connect("https://api.instagram.com/v1/users/" . $user_id . "/media/recent?&access_token=" . $access_token . "&count=4");
    

  }
  ?>

  <?php if (is_array($return->data) && !empty($return->data)): ?>
    <div class="links-img">
      <?php foreach ($return->data as $data): ?>
        <a class="link-img" href="<?php echo $data->link ?>">
          <img src="<?php echo $data->images->standard_resolution->url ?>" alt=""
               width="360" height="360">
        </a>

      <?php endforeach; ?>


    </div>
  <?php endif; ?> 





</div>