<?php 
$section_id = get_sub_field('section_id');
?>

<?php if (have_rows('items')): ?>

  <div class="description-block" 
<?php if ($section_id): ?>
       id="<?php echo $section_id ?>"
     <?php endif; ?>>
    <div class="container">
      <?php
      while (have_rows('items')) : the_row();

        $image = get_sub_field('image');
        ?>


        <div class="block-holder">
          <?php if ($image): ?>
            <div class="img-holder">
              <?php
              am_the_retina_img($image, 'cw594h469');
              ?>
            </div>
          <?php endif; ?>
          <div class="text-holder">
            <div class="frame">
              <?php am_the_sub_field('title', '<h2 class="animated-bottom">', '</h2>') ?>
              <?php am_the_sub_field('text', '<p class="animated-bottom">', '</p>') ?>
            </div>
          </div>
        </div>


      <?php endwhile;
      ?>

    </div>
  </div>
  <?php
endif;
?> 