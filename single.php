<?php get_header(); ?>

<div class="main-block">
  <div class="container">
    <div id="content">

		<?php if (have_posts()) : ?>

			<?php while (have_posts()) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'post' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; ?>

		<?php else : ?>
		
			<?php get_template_part( 'template-parts/content', 'none' ); ?>
			
		<?php endif; ?>

	 </div><!-- content -->

    <?php get_sidebar('content') ?>
  </div>
</div>

<?php get_footer(); ?>