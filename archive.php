<?php get_header(); ?>

<div class="main-block">
  <div class="container">
    <div id="content">
	 <div class="title">
		<?php
			the_archive_title( '<h1 class="page-title">', '</h1>' );
			the_archive_description( '<div class="taxonomy-description">', '</div>' );
		?>
        </div><!-- /title -->

		<?php if (have_posts()) : ?>

			<div class="posts-list">

					<?php while (have_posts()) : the_post(); ?>

						<?php get_template_part( 'template-parts/content', 'post' ); ?>

					<?php endwhile; ?>

				</div>

			<?php get_template_part( 'template-parts/pagination', 'post' ); ?>

		<?php else : ?>
			<?php get_template_part( 'template-parts/content', 'none' ); ?>
		<?php endif; ?>

	  </div><!-- content -->

<?php get_sidebar('content') ?>
  </div>
</div>

<?php get_footer(); ?>